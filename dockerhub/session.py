import requests

from dockerhub.exceptions import DockerhubException

DOCKERHUB_API_URL = "https://hub.docker.com"


class DockerhubSession(requests.Session):
    def __init__(self, username: str, password_or_token: str):
        super().__init__()
        self.username = username
        self.password_or_token = password_or_token
        self.__authenticated = False

    def authenticate(self):
        response = requests.post(
            DOCKERHUB_API_URL + "/v2/users/login",
            data={"username": self.username, "password": self.password_or_token},
        )
        if response.status_code != 200:
            try:
                error_message = response.json()["detail"]
            except Exception:
                error_message = "An unexpected error occured during login"
            raise DockerhubException(error_message)

        access_token = response.json()["token"]
        self.headers.update({"Authorization": f"Bearer {access_token}"})

    def absolute_url(self, relative_url: str) -> str:
        if relative_url.startswith("http"):
            return relative_url
        return DOCKERHUB_API_URL + "/" + relative_url.lstrip("/")

    def request(self, method: str, relative_url: str, **options):
        if not self.__authenticated:
            self.authenticate()
        self.__authenticated = True

        response = super().request(method, self.absolute_url(relative_url), **options)
        if response.status_code >= 400:
            try:
                error_message = response.json()["detail"]
            except Exception:
                error_message = "An unexpected error occured during login"
            raise DockerhubException(error_message)
        return response
