from dockerhub.session import DockerhubSession
from dockerhub.models import RepositoryDTO

class DockerhubClient:
    def __init__(self, username: str, password_or_token: str):
        self.session = DockerhubSession(username, password_or_token)

    def get_repository(self, repo_path: str) -> RepositoryDTO:
        response = self.session.get(f"/v2/repositories/{repo_path}/")
        response_data = response.json()
        return RepositoryDTO.parse_obj(response_data)

    def update_repository(self, repo_path: str, description: str = None, full_description: str = None, full_description_path: str = None, is_private: bool=None):        
        if full_description_path:
            with open(full_description_path, encoding="utf-8") as readme:
                full_description = readme.read()
        
        repository = self.get_repository(repo_path)
        #
        print("patched data", {
            "description": repository.description if description is None else description,
            "full_description": repository.full_description if full_description is None else full_description,
            "is_private": repository.is_private if is_private is None else is_private
        })
        response = self.session.patch(f"/v2/repositories/{repo_path}/", data={
            "description": repository.description if description is None else description,
            "full_description": repository.full_description if full_description is None else full_description,
            "is_private": repository.is_private if is_private is None else is_private
        })
        return response.json()
