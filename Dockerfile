FROM python:3.8-alpine

ARG DOCKERHUB_CLIENT_VERSION

RUN pip install --no-cache-dir dockerhub-client==$DOCKERHUB_CLIENT_VERSION

ENTRYPOINT [ "dockerhub" ]
