from datetime import datetime
from pydantic import BaseModel
from typing import Optional
class PermissionDTO(BaseModel):
    read: bool
    write: bool
    admin: bool

class RepositoryDTO(BaseModel):
    user: str
    name: str
    namespace: str
    repository_type: str
    status: int
    description: str
    is_private: bool
    is_automated: bool
    can_edit: bool
    star_count: int
    pull_count: int
    last_updated: datetime
    is_migrated: bool
    collaborator_count: int
    affiliation: str
    hub_user: str
    has_starred: bool
    full_description: Optional[str]
    permissions: PermissionDTO